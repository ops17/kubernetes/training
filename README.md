# training

培训内容

第一讲：通过单体应用拆分到微服务、linux虚拟化技术，ns和cgroup对其隔离和限制，流行的docker和rkt、
       k8s组件概览，管理和工作node区别，以及各个自带组件。

第二讲：介绍docker打包、管理、托管仓库、运行。介绍k8s几种安装方式。部署简单应用在k8s上。